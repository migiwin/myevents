import 'package:flutter/material.dart';
import 'add_item_dialog.dart';
import 'to_do_item.dart';
import 'detail_screen.dart';
import 'event.dart';

void main() => runApp(MaterialApp(home: ToDo()));

class ToDo extends StatefulWidget {
  @override
  _ToDoState createState() => _ToDoState();
}

class _ToDoState extends State<ToDo> {
  //Event event = new Event("jo", "odrs", "efdsa", "aefds");
  Map<String, bool> items = {'Geburtstag Thomas': false, 'Thomas Zahnarzt': false, 'Essen mit Anna': false, 'Schulabschluss': false ,'Thomas nach Hause bringen': false};

  void addItem(String item) {
    setState(() {
      items[item] = false;
    });
    Navigator.of(context).pop();
  }

  void deleteItem(String key) {
    setState(() {
      items.remove(key);
    });
  }

  void toggleDone(String key) {
    setState(() {
      items.update(key, (bool done) => !done);
    });
  }

  void newEntry() {
    showDialog<AlertDialog>(
        context: context,
        builder: (BuildContext context) {
          return AddItemDialog(addItem);
        }
    );
  }

  void showLocation(){

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("To-Do-App"),
        backgroundColor: Color.fromRGBO(35, 152, 185, 100),
      ),
      body:
      ListView.builder(

        itemCount: items.length,
        itemBuilder: (context, i) {
          String key = items.keys.elementAt(i);
          return ToDoItem(
            key,
            items[key],
                () => deleteItem(key),
                () => toggleDone(key),
          );
        },
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: newEntry,
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(35, 152, 185, 100),
      ),

    );
  }
}



